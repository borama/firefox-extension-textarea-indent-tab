const input = document.forms[0].spaces;
const storage = browser.storage.local;

const key = 'spaces';
const { [key]: spaces = 4 } = await storage.get(key);
input.value = spaces;

input.addEventListener('change', async () => {
  await storage.set({ [key]: input.value });
});

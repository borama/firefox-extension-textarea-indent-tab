document.addEventListener('keydown', async function(event) {
    // ignore all keypresses which are not Tabs, or have Ctrl or Alt or Meta pressed
    if (event.keyCode != 9 || event.ctrlKey || event.altKey || event.metaKey) return;

    var target = event.target;
    // ignore all keypresses not inside textareas
    if (!/textarea/i.test(target.nodeName)) return;

    var start = target.selectionStart;
    var end = target.selectionEnd;
    // ignore events when no text is selected
    if (start == end) return;

    var text = target.value.substring(start, end);
    // ignore text selections that do not span several lines
    if (!/[\n\r]/.test(text)) return;

    // prevent moving focus to the next control
    event.preventDefault();

    const { spaces } = (await browser.storage.local.get('spaces'))
    const indent = ' '.repeat(spaces);

    if (!event.shiftKey) {
        // when shift is not pressed insert spaces after every newline
        text = indent + text.replace(/([\r\n])(?!$)/gm, `$1${indent}`);
    } else {
        // when shift is pressed remove spaces after every newline
        text = text.replace(new RegExp(`(^|[\r\n]+)${indent}`, 'g'), '$1');
    }

    // construct the new value for textarea
    target.value = target.value.substring(0, start) + text + target.value.substring(end);
    // and restore the selection
    target.selectionStart = start;
    target.selectionEnd = start + text.length;
}, false);

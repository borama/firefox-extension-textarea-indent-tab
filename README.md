# Indent lines in textarea with Tab

## a Firefox extension

This extension allows you to indent lines of text in textarea using Tab,
and un-indent them using Shift+Tab. Configure the number of spaces (4 by default)
in the options page.

## Installation

[Indent lines in textarea with Tab on Firefox Addons](https://addons.mozilla.org/de/firefox/addon/textarea-indent-tab/)

## License

This code is licensed under [Mozilla Public License version 2](https://www.mozilla.org/en-US/MPL/2.0/)
